from datetime import datetime

class Schedule:

    token_key = "token"
    hosts_key = "hosts"
    tasks_key = "tasks"
    datetime_key = "datetime"
    datetime_fmt = "%Y-%m-%d %H:%M:%S.%f"

    def __init__(self, tasks):
        self.tasks = tasks

    def verify(self):
        if Schedule.token_key not in self.tasks or not isinstance(self.tasks[Schedule.token_key], str):
            return False

        if Schedule.hosts_key not in self.tasks or not isinstance(self.tasks[Schedule.hosts_key], list):
            return False
        for host in self.tasks[Schedule.hosts_key]:
            if not isinstance(host, str):
                return False

        if Schedule.tasks_key not in self.tasks or not isinstance(self.tasks[Schedule.tasks_key], list):
            return False
        for task in self.tasks[Schedule.tasks_key]:
            if not isinstance(task, dict) or len(task.keys()) != 1:
                return False
        return True

    def get_schedule(self):
        return self.tasks

    def set_time(self):
        time_now = datetime.utcnow()
        self.tasks[Schedule.datetime_key] = time_now.strftime(Schedule.datetime_fmt)

    def get_time(self):
        return datetime.strptime(self.tasks[Schedule.datetime_key], Schedule.datetime_fmt)

    def get_token(self):
        return self.tasks[Schedule.token_key]

    def get_hosts(self):
        return self.tasks[Schedule.hosts_key]

    def get_tasks(self):
        return self.tasks[Schedule.tasks_key]
