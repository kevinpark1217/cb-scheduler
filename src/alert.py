from datetime import datetime

class Alert:

    alerts = {}
    alert_time = "time"
    alert_msg = "msg"

    def set_alert(id, msg):
        # id=str(id)
        Alert.alerts[id] = {}
        Alert.alerts[id][Alert.alert_time] = datetime.utcnow()
        Alert.alerts[id][Alert.alert_msg] = msg

    def get_alert(id):
        # id=str(id)
        # print(Alert.alerts)
        if not id in Alert.alerts:
            return None
        curr = datetime.utcnow()
        elapsed = curr - Alert.alerts[id][Alert.alert_time]
        elapsed_hours = elapsed.seconds // 3600
        if elapsed_hours < 24:
            return Alert.alerts[id]
        del Alert.alerts[id]
        return None
