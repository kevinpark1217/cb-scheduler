import json

class InvalidConfigException(Exception):
   """Fails config verification"""
   pass

class Config:

    def __verify_conn(self):
        if 'server' in self.config:
            server_config = self.config['server']
            if 'host' in server_config and 'port' in server_config:
                return True
        return False

    def __verify_rest(self):
        if 'rest-api' in self.config:
            rest_config = self.config['rest-api']
            if 'host' in rest_config and 'port' in rest_config:
                return True
        return False

    def __verify_scheduler(self):
        if 'schedule' in self.config:
            return True
        return False

    def __verify(self):
        if not self.__verify_conn():
            return False
        if not self.__verify_rest():
            return False
        if not self.__verify_scheduler():
            return False
        return True

    def __init__(self, fpath):
        self.fpath = fpath

    def load(self):
        with open(self.fpath, 'r') as fh:
            self.config = json.loads(fh.read())
        if not self.__verify():
            raise InvalidConfigException

    def save(self):
        if not self.__verify():
            raise InvalidConfigException
        with open(self.fpath, 'w') as fh:
            json.dump(self.config, fh, indent=4)
