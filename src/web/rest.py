from flask import Flask, request, jsonify, make_response
from flask_restful import Api, Resource, reqparse
import random
import socket
import os

from src import Schedule, Alert
from src.sys import Retriever
from src.sys import InvalidFileException, InvalidTaskIdException, InvalidHostException, InvalidActionException


class Rest:

    temp_tasks = {}

    def __init__(self, rest_config):
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.rest_config = rest_config

    def run(self):
        self.api.add_resource(RestSchedule, "/schedule")
        self.api.add_resource(RestScheduleList, "/schedule/<int:id>")
        self.api.add_resource(RestScheduleAction, "/schedule/<int:id>/<string:host>")
        host = socket.gethostname()
        if self.rest_config['host']:
            host = self.rest_config['host']
        self.app.run(debug=False, host=host, port=self.rest_config['port'])


class RestSchedule(Resource):

    def get(self):
        resp = {}
        resp["msg"] = "List of Partially and Fully Ran Tasks"
        resp["id"] = Retriever.get_tasks()
        return make_response(jsonify(resp), 200)

    def post(self):
        resp = {}
        data = request.get_json(force=True)
        schedule = Schedule(data)
        if not schedule.verify():
            resp["msg"] = "Invalid Schedule JSON Format"
            return make_response(jsonify(resp), 400)

        # auth = Auth(schedule.get_token())
        # if not auth.validate():
        #     resp["msg"] = "Unauthorized Carbon Black Response Token"
        #     return make_response(jsonify(resp), 400)
        # for host in schedule.get_hosts():
        #     if not auth.valid_host(host):
        #         resp["msg"] = "Host does not exist"
        #         resp["host"] = host
        #         return make_response(jsonify(resp), 400)

        schedule.set_time()
        nonce = random.randint(1,2**32-1)
        Rest.temp_tasks[nonce] = schedule.get_schedule()
        resp["msg"] = "Schedule Sucessfully Added"
        resp["id"] = nonce
        return make_response(jsonify(resp), 200)


class RestScheduleList(Resource):

    def get(self, id):
        resp = {}

        alert = Alert.get_alert(id)
        if alert:
            resp["alert"] = alert

        try:
            hosts = Retriever.get_hosts(id)
            resp["msg"] = "List of hosts that ran the schedule"
            resp["hosts"] = hosts
            return make_response(jsonify(resp), 200)
        except InvalidTaskIdException:
            resp["msg"] = "Invalid Task ID"
            return make_response(jsonify(resp), 400)


class RestScheduleAction(Resource):

    def get(self, id, host):
        parser = reqparse.RequestParser()
        parser.add_argument("action", required=True, help="Specify an action")
        parser.add_argument("file", help="File to retrieve")
        args = parser.parse_args()

        resp = {}
        action = args["action"]
        try:
            content = Retriever.get_action(id, host, **args)
            if isinstance(content, list):
                resp["msg"] = "List of {}".format(action)
                resp[action] = content
                return make_response(jsonify(resp), 200)
            return content, 200
        except InvalidTaskIdException:
            resp["msg"] = "Invalid Task Id {}".format(id)
            return make_response(jsonify(resp), 400)
        except InvalidHostException:
            resp["msg"] = "Invalid Hostname {}".format(host)
            return make_response(jsonify(resp), 400)
        except InvalidActionException:
            resp["msg"] = "Invalid Action {}".format(action)
            return make_response(jsonify(resp), 400)
        except InvalidFileException:
            resp["msg"] = "Invalid File {}".format(args["file"])
            return make_response(jsonify(resp), 400)
