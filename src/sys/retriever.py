from src.sys import const
import os
import os.path


class InvalidTaskIdException(Exception):
    """Invalid Task ID"""
    pass

class InvalidHostException(Exception):
    """Invalid Hostname Associated with the Task"""
    pass

class InvalidActionException(Exception):
    """Invalid Action"""
    pass

class InvalidFileException(Exception):
    """Invalid File Requested"""
    pass


class Retriever:

    def get_tasks():
        return os.listdir(const.path)

    def __get_id_path(id):
        path = os.path.join(const.path, str(id))
        if not os.path.exists(path):
            raise InvalidTaskIdException
        return path

    def __get_host_path(id, host):
        path = Retriever.__get_id_path(id)
        path = os.path.join(path, host)
        if not os.path.exists(path):
            raise InvalidHostException
        return path

    def __get_files(path, file):
        path = os.path.join(path, const.files_path)
        if not os.path.exists(path):
            raise InvalidActionException
        if not file:
            return os.listdir(path)

        # File argument supplied
        path = os.path.join(path, file)
        if not os.path.isfile(path):
            raise InvalidFileException
        with open(path, "r") as fh:
            return fh.read()

    def __get_registry(path, _):
        path = os.path.join(path, const.registry_file)
        if not os.path.isfile(path):
            raise InvalidActionException
        with open(path, "r") as fh:
            return fh.read()

    def __get_commands(path, _):
        path = os.path.join(path, const.commands_file)
        if not os.path.isfile(path):
            raise InvalidActionException
        with open(path, "r") as fh:
            return fh.read()

    def __invalid_action(_):
        raise InvalidActionException

    def get_hosts(id):
        path = Retriever.__get_id_path(id)
        return os.listdir(path)

    def get_action(id, host, action, file):
        path = Retriever.__get_host_path(id, host)

        action_map = {
            "files": Retriever.__get_files,
            "registry": Retriever.__get_registry,
            "commands": Retriever.__get_commands
        }

        func = action_map.get(action, Retriever.__invalid_action)
        return func(path, file)
