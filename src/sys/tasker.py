from cbapi.response import CbResponseAPI, Process, Binary, Sensor
from cbapi.live_response_api import LiveResponseError
import os
import os.path

from src import Schedule, Alert
from src.sys import const

class Tasker:

    def __init__(self, id, schedule):
        self.cb = CbResponseAPI(url=os.environ['cb-url'], token=schedule.get_token(), ssl_verify=False)
        self.path = os.path.join(const.path, str(id))
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        self.schedule = schedule
        self.timeout = 60
        self.id = id

    def __get_sensor(self, hostname):
        host_str = "hostname:{}".format(hostname)
        return self.cb.select(Sensor).where(host_str).first()

    def __list_online(self, hosts):
        alive = []
        for host in hosts:
            sensor = self.__get_sensor(host)
            if not sensor or sensor.hostname.casefold() != host.casefold():
                Alert.set_alert(self.id, "Invalid host {} has been removed".format(host))
                print("Invalid hostname {}".format(host))
                hosts.remove(host)
            elif sensor.status == "Online":
                alive.append(host)
        return alive

    def __get_file(self, file):
        save_path = os.path.join(self.host_path, const.files_path)
        if not os.path.exists(save_path):
            os.makedirs(save_path)

        basename = os.path.basename(file)
        fname = os.path.join(save_path, basename)
        fh = open(fname, 'wb')
        try:
            content = self.lr_session.get_file(file, timeout=self.timeout)
            fh.write(content)
        except LiveResponseError as e:
            fh.write(str.encode(str(e)))
        fh.close()

    def __get_registry(self, reg):
        fname = os.path.join(self.host_path, const.registry_file)
        fh = open(fname, 'w')
        try:
            reg_result = self.lr_session.get_registry_value(reg)
        except LiveResponseError as e:
            reg_result = str(e)
        reg_out = "{}\n{}\n{}\n\n".format(reg, '-'*100, reg_result)
        fh.write(reg_out)
        fh.close()

    def __run_command(self, cmd):
        fname = os.path.join(self.host_path, const.commands_file)
        fh = open(fname, 'w')
        try:
            full_cmd = "cmd.exe /C {}".format(cmd)
            cmd_result = self.lr_session.create_process(full_cmd, wait_for_output=True, wait_timeout=self.timeout)
        except LiveResponseError as e:
            cmd_result = str(e)
        cmd_out = "{}\n{}\n{}\n\n".format(cmd, '-'*100, cmd_result)
        fh.write(cmd_out)
        fh.close()

    def __run(self, host):
        # Live session
        sensor = self.__get_sensor(host)
        self.lr_session = sensor.lr_session()

        # Prep save directory
        self.host_path = os.path.join(self.path, host)
        if not os.path.exists(self.host_path):
            os.makedirs(self.host_path)

        task_map = {
            "file": self.__get_file,
            "registry": self.__get_registry,
            "command": self.__run_command
        }

        tasks = self.schedule.get_tasks()
        for task in tasks:
            type = [*task][0]
            func = task_map.get(type, lambda _: None)
            func(task[type])

        self.lr_session.close()

    def run(self):
        hosts = self.schedule.get_hosts()
        alive_hosts = self.__list_online(hosts)
        is_full = True if len(alive_hosts) == len(hosts) else False
        print("Online: {}/{}".format(len(alive_hosts), len(hosts)))

        for alive_host in alive_hosts:
            print("Processing {}...".format(alive_host))
            self.__run(alive_host)
            hosts.remove(alive_host)

        return is_full
