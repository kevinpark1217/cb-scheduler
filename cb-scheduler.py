from cbapi.errors import UnauthorizedError, ServerError
from datetime import datetime
import threading
import signal
import time
import os

from src import Config, Schedule, Alert
from src.sys import Tasker, const
from src.web import Rest
# from multiprocessing import Pool, TimeoutError

# import os.path
# import os
# import subprocess
#
# creds_path = os.getenv('USERPROFILE') + "\\.carbonblack\\credentials.response"
# script_path = "C:/Program Files/Python37/Scripts/cbapi-response"
# if not os.path.isfile(creds_path):
#     print("Configuring Carbon Black API...", end="")
#     server_config = config['server']
#     p = subprocess.Popen(['python', script_path, 'configure'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#
#     server_input = "https://{}:{}".format(server_config['host'], server_config['port']) + os.linesep
#     server_input += "N"+ os.linesep
#     server_input += server_config['api-key'] + os.linesep
#     p.communicate(input=server_input.encode())[0]
#     print("Done")

is_stop = False
def signal_handler(sig, frame):
    global is_stop
    is_stop = True
    print('Gracefully stopping the scheduler...')

signal.signal(signal.SIGINT, signal_handler)

cl = Config('config.json')
cl.load()
config = cl.config

server_config = config['server']
os.environ['cb-url'] = "https://{}:{}".format(server_config['host'], server_config['port'])

rest_config = config['rest-api']
resty = Rest(rest_config)
resty_thread = threading.Thread(target=resty.run, args=(), daemon=True)
resty_thread.start()

time.sleep(2.5) # Delayed start
if not os.path.exists(const.path):
    os.makedirs(const.path)

while not is_stop:
    schedule_config = config['schedule']
    for key in [*schedule_config]:
        if is_stop:
            break
        schedule = Schedule(schedule_config[key])
        date_created = schedule.get_time()
        date_diff = datetime.utcnow() - date_created
        if date_diff.days > 30:
            del schedule_config[key]
            Alert.set_alert(key, "Task has been deleted after 30 days")
            print("Deleted Old Task: {}", key)
            continue

        try:
            tasker = Tasker(key, schedule)
        except UnauthorizedError:
            del schedule_config[key]
            Alert.set_alert(key, "Task has been removed for invalid CB token")
            print("Invalid Token. Removed Task {}...".format(key))
            continue

        print("Starting Task {}...".format(key))
        try:
            if tasker.run():
                print("Completed Task {}...".format(key))
                del schedule_config[key]
            else:
                print("Partially Completed Task {}...".format(key))
        except ServerError:
            Alert.set_alert(key, "Task has been removed for insufficient CB token permission")
            print("Not enough token permission")
            del schedule_config[key]
            os.rmdir(tasker.path)
        except Exception as e:
            print("Failed Task {}...".format(key))
            print(e)

    wait_period = 60
    wait_interval = 5
    t = 0
    while t < wait_period:
        time.sleep(wait_interval)
        t += wait_interval

        # Merge temp_task with task_config
        schedule_config.update(Rest.temp_tasks)

        Rest.temp_tasks = {}
        cl.save()
        if is_stop:
            break
